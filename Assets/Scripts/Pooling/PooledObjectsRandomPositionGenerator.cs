﻿using System;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(ObjectsPool))]
public class PooledObjectsRandomPositionGenerator : MonoBehaviour
{
    [SerializeField]
    public int pooledObjectsMaxCount; // can be moved to base generator class?

    [SerializeField]
    public GameObject prefab;

    [SerializeField]
    private int milliSecondsForNewObjectGeneration;
    
    private ObjectsPool objectsPool;

    private async Task Start()
    {
        objectsPool = GetComponent<ObjectsPool>();
        objectsPool.InitializePool(pooledObjectsMaxCount, prefab);
        
        await ConstantGenerateObjects();
    }

    private async Task ConstantGenerateObjects()
    {
        while (true)
        {
            // TODO: check if all pooled objects are enabled first and only then get vectors
            Vector3 screenRandomPosition = new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height));
            Vector3 worldRandomPosition = Camera.main.ScreenToWorldPoint(screenRandomPosition);

            try
            {
                objectsPool.SpawnFromPool(worldRandomPosition);
            }
            catch (InvalidOperationException) // all pool objects are currently active, ignore this type of exception
            {
            }
            await Task.Delay(milliSecondsForNewObjectGeneration);
        }

    }
}
