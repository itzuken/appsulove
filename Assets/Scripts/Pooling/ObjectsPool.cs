﻿using System.Linq;
using UnityEngine;

/// <summary>
/// A component to be used by object generators that allows generated objects to be reused.
/// </summary>
public class ObjectsPool : MonoBehaviour
{
    private GameObject[] objectsInPool;

    public void InitializePool(int poolSize, GameObject prefab)
    {
        objectsInPool = new GameObject[poolSize];

        for (int i = 0; i < poolSize; i++)
        {
            GameObject obj = Instantiate(prefab);
            obj.SetActive(false);
            objectsInPool[i] = obj;
        }
    }

    public GameObject SpawnFromPool(Vector2 position)
    {
        GameObject gameObj = objectsInPool.FirstOrDefault(go => !go.active); // TODO: add queue instead of using list + First()? list for enabled and disabled?
        if (gameObj == null)
        {
            throw new System.InvalidOperationException("All elements in the pool are active");
        }

        gameObj.SetActive(true);
        gameObj.transform.position = position;

        return gameObj;
    }
}

