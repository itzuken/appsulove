﻿using UnityEngine;
using UnityEngine.UI;

public class CollisionScoreCounter : MonoBehaviour
{
    private Text scoreUiText;

    private int score;

    private void Start()
    {
        scoreUiText = GameObject.Find("Score").GetComponent<Text>(); // TODO: CHANGE THIS! not to be searched by name
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Square")) //TODO: remove the magic string
        {
            collision.gameObject.SetActive(false);
            scoreUiText.text = $"Score: {++score}"; // change string usage?
        }
    }
}

