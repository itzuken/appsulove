﻿using System;
using UnityEngine;

[RequireComponent(typeof(MoveTowardsPoint))]
public class PlayerInputHandler : MonoBehaviour
{
    private IInputReceiver inputReceiver;
    private MoveTowardsPoint moveTowardsPoint;

    private void Start()
    {
#if UNITY_STANDALONE // For win/linux/mac build
        inputReceiver = gameObject.AddComponent<MouseInputReceiver>();
#endif

        if (inputReceiver == null)
        {
            throw new Exception($"Input method is not defined, {nameof(inputReceiver)} is not initialized.");
        }
        moveTowardsPoint = GetComponent<MoveTowardsPoint>();
        
        inputReceiver.OnClick += (Vector2 point) =>
        {
            //Vector2 playerRay = Camera.main.ScreenToWorldPoint(point);
            RaycastHit2D playerHit = Physics2D.Raycast(point, Vector2.zero);

            if (playerHit.collider?.gameObject != gameObject)
            {
                moveTowardsPoint.Move(point);
            }
            else
            {
                moveTowardsPoint.Stop();
            }
        };
    }
}