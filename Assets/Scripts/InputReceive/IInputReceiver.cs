﻿using System;
using UnityEngine;

/// <summary>
/// An interface for different input methods.
/// </summary>
public interface IInputReceiver // Using an interface instead of an abstract class for better testability
{
    /// <summary>
    /// Click event for different types of input.
    /// </summary>
    event Action<Vector2> OnClick;
}
