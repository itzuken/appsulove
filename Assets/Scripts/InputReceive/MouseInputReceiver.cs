﻿using System;
using UnityEngine;

public class MouseInputReceiver : MonoBehaviour, IInputReceiver
{
    public event Action<Vector2> OnClick;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            OnClick(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
    }
}
