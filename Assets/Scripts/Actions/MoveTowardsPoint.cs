﻿using System;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// A component for smoothly movement towards some point.
/// </summary>
public class MoveTowardsPoint : MonoBehaviour // TODO: rename
{
    [SerializeField]
    private float speed;

    [SerializeField]
    private float accuracy;

    private Coroutine moveCoroutine;

    public void Move(Vector2 destinationPoint) //avoiding using Update()
    {
        // get_transform can only be called from the main thread, using a coroutine
        if (moveCoroutine == null)
        {
            moveCoroutine = StartCoroutine(MoveCoroutine(destinationPoint));
        }
    }

    public void Stop()
    {
        if (moveCoroutine != null)
        {
            StopCoroutine(moveCoroutine);
            moveCoroutine = null;
        }
    }

    private IEnumerator MoveCoroutine(Vector2 destinationPoint)
    {
        while (Vector2.Distance(transform.position, destinationPoint) > accuracy)
        {
            transform.position = Vector2.Lerp(transform.position, destinationPoint, Time.deltaTime * speed);
            yield return null;
        }
        moveCoroutine = null;
    }
}

